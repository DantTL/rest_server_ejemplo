package com.ejemplo.restserverejemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestServerEjemploApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestServerEjemploApplication.class, args);
	}

}
